// DPlayWrapper.cpp : Defines the exported functions for the DLL.
//

#include "pch.h"
#include "framework.h"
#include "DPlayWrapper.h"


// This is an example of an exported variable
DPLAYWRAPPER_API int nDPlayWrapper=0;

// This is an example of an exported function.
DPLAYWRAPPER_API int fnDPlayWrapper(void)
{
    return 0;
}

// This is the constructor of a class that has been exported.
CDPlayWrapper::CDPlayWrapper()
{
    return;
}
