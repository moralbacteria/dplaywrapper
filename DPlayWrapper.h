// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the DPLAYWRAPPER_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// DPLAYWRAPPER_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef DPLAYWRAPPER_EXPORTS
#define DPLAYWRAPPER_API __declspec(dllexport)
#else
#define DPLAYWRAPPER_API __declspec(dllimport)
#endif

// This class is exported from the dll
class DPLAYWRAPPER_API CDPlayWrapper {
public:
	CDPlayWrapper(void);
	// TODO: add your methods here.
};

extern DPLAYWRAPPER_API int nDPlayWrapper;

DPLAYWRAPPER_API int fnDPlayWrapper(void);
